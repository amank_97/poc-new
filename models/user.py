from database.db import db

#############################################################################################################
class UserModel(db.Model):
    __tablename__ = "users"
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(80))
    password = db.Column(db.String())
    todos=db.relationship('Todo', backref='user')

    def __init__(self, username, password ):
        self.username = username
        self.password = password
        

    def json(self):
        return {
            "id": self.id,
            "username": self.username
        }, 200

    # Method to save user to DB
    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    # Method to remove user from DB
    def remove_from_db(self):
        db.session.delete(self)
        db.session.commit()

    # Class method which finds user from DB by username
    @classmethod
    def find_user_by_username(cls, username):
        return cls.query.filter_by(username=username).first()

    # Class method which finds user from DB by id
    @classmethod
    def find_user_by_id(cls, _id):
        return cls.query.filter_by(id=_id).first()

###################  Model for the task ######################################################################

class Todo(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    task = db.Column(db.String(500))
    user_id=db.Column(db.Integer, db.ForeignKey('users.id'))
    comments=db.relationship('Comment', backref='task')

    def __init__(self, task, user_id):
        self.task = task
        self.user_id= user_id
        
    def json(self):
        return {
            "id": self.id,
            "task": self.task,
            "user_id":self.user_id
            
        }, 200

    # Method to save user to DB
    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    # Method to remove user from DB
    def remove_from_db(self):
        db.session.delete(self)
        db.session.commit()

    # Class method which finds task from DB by user_id
    @classmethod
    def find_user_by_username(cls, user_id):
        return cls.query.filter_by(user_id=user_id).first()

    # Class method which finds user from DB by id
    @classmethod
    def find_user_by_id(cls, _id):
        return cls.query.filter_by(id=_id).first()


#######################  Now for multiple comments on task ###################################################
class Comment(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    comment = db.Column(db.String(500))
    task_id=db.Column(db.Integer, db.ForeignKey('todo.id'))

    def __init__(self, comment, task_id):
        self.comment = comment
        self.task_id= task_id

    def json(self):
        return {
            "id": self.id,
            "comment": self.comment,
            "task_id":self.task_id
            
        }, 200

    # Method to save user to DB
    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    # Method to remove user from DB
    def remove_from_db(self):
        db.session.delete(self)
        db.session.commit()

    # Class method which finds comments from DB by task_id
    @classmethod
    def find_user_by_username(cls, task_id):
        return cls.query.filter_by(task_id=user_id).first()

    # Class method which finds user from DB by id
    @classmethod
    def find_user_by_id(cls, _id):
        return cls.query.filter_by(id=_id).first()
